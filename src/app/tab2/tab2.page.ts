import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { MessageServiceService } from '../message-service.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  lastMessages = [];
  constructor(private messageServ: MessageServiceService, public alertController: AlertController, public toastController: ToastController) {
    this.fecthData();
  }

  async showToast(Message: string='N/A', TypeToasts: string='primary'){
    const toast = await this.toastController.create({
      header: 'Message',
      message: Message,
      position: 'top',
      color: TypeToasts,
      duration: 3000,
    });

    await toast.present();
  }

  filterItems(event){
    const val = event.target.value;
    if(val && val.trim() != ''){
      this.lastMessages = this.lastMessages.filter((item)=>{
        return (item.code.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    } else {
      this.fecthData();
    }
  }

  async deleteMessage(index){
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Are your sure',
      message: 'This is irreversible',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.messageServ.deleteMessage(index);
            this.showToast('Number was delete', 'success');
            setTimeout( () => {
              this.fecthData();
            }, 500 );
          }
        }
      ]
    });

    await alert.present();
  }

  useAgain(phone){
    let Uri = "whatsapp://send?phone="+phone+"";
    window.open(Uri, "_system", "location=yes");
  }

  fecthData(){
    this.lastMessages = [];
    this.messageServ.getLastMessage().then((data:any)=>{
      this.lastMessages = data;
      console.log(this.lastMessages);
    });
  }

}

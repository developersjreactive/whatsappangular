import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class MessageServiceService {

  messages: any = [];

  constructor(private storage: Storage) {
    this.storage.create();
   }

  deleteMessage(index){
    this.getLastMessage().then((r:any)=>{
      console.log(r);
      if(!r){
        this.messages = [];
      } else {
        this.messages = r;
      }
      
      this.messages.splice(index, 1);
      return this.storage.set("LastMessageWPD", this.messages);
    });
  }

  saveMessage(data:any){
    //this.messages.push(data);
    //return this.storage.set("LastMessageWPD", this.messages);
    this.getLastMessage().then((r:any)=>{
      console.log(r);
      if(!r){
        this.messages = [];
      } else {
        this.messages = r;
      }
      
      this.messages.push(data);
      return this.storage.set("LastMessageWPD", this.messages);
    });
  }

  getLastMessage(){
    return new Promise(resolve => {
      this.storage.get('LastMessageWPD').then( (last) => {
        resolve(last);
      } );
    });
  }
}

import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { MessageServiceService } from '../message-service.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  code : string = "";
  numberphone: string = "";
  message: string = "";

  constructor(public toastController: ToastController, private messageServ: MessageServiceService) {

  }

  sendmessage(){

    let forms = {
      code: this.code,
      phone: this.numberphone,
      msg: this.message
    };

    if(forms.code == undefined || forms.code == ""){
      this.showToast('Please write a code', 'warning');
      return false;
    }

    if(forms.phone == undefined || forms.phone == ""){
      this.showToast('Please write phone', 'warning');
      return false;
    }

    if(forms.msg == undefined || forms.msg == ""){
      this.showToast('Please write a message', 'warning');
      return false;
    }

    let Uri = "whatsapp://send?phone=+"+forms.code+""+forms.phone+"&text="+forms.msg;

    this.messageServ.saveMessage(forms);

    window.open(Uri, "_system", "location=yes");

  }

  async showToast(Message: string='N/A', TypeToasts: string='primary'){
    const toast = await this.toastController.create({
      header: 'Message',
      message: Message,
      position: 'top',
      color: TypeToasts,
      duration: 3000,
    });

    await toast.present();
  }

}
